Tweetorowa katastrofa
=======================================================


Cel zadania:
-------------------------
Fanpage facebookowy „Konflikty i Katastrofy Światowe – KIKŚ” chce zautomatyzować proces zdobywania informacji o pojawiających się na świecie problemach. Jeden z administratorów strony zauważył, że świetnym źródłem najnowszych wiadomości są wiadomości twitterowe – w końcu jak długo zajmuje napisanie 160 znaków o tym, że gdzieś wystąpiła sytuacja kryzysowa? Pojawił się jednak pewien problem – nie zawsze bowiem coś, co postujący uznał za katastrofę faktycznie nią jest. Co więcej, część informacji celowo była fałszowana, by wywołać panikę lub w innych, niecnych celach. Dysponując zbiorem danych zawierających twitty o przeszłych katastrofach oraz te zawierające nieprawdziwe informacje, Twoim zadaniem jest stworzenie klasyfikatora, który będzie poprawnie rozróżniał fake newsy od faktycznych informacji o katastrofach. Administrator strony marzy również o generatorze podsumowań, który będzie tworzył krótką notatkę prasową na podstawie kilku twittów… ale to już może okazać się poza spektrum tego projektu.


TL;DR:
-------------------------
Klasyfikowanie tweetów (o aktualnych wydarzeniach na świecie) jako fake newsow lub nie.


Opis zbioru:
-------------------------
**id** – identyfikator liczbowy danego twitta;

**text** – treść twitta;

**location** – lokalizacja, z której twitt został wysłany (puste w przypadku gdy wysyłający nie miał udostępnionej lokalizacji w telefonie lub komputerze);

**keyword** – wyróżnione słowa kluczowe (mogą być puste);

**target** – w zbiorze treningowym, zmienna logiczna opisująca prawdziwość przekazywanej informacji (1), lub (0) dla fake newsów i informacji nieistotnych.

Proponowane metody:
-------------------------
1. klasyfikator Bayesowski,
2. *(Dominik)*.

Wykorzystane metryki:
-------------------------

W obecych czasach, kluczową rolę odgrywa informacja. Do tego stopnia, że prowadzi się wojny informacyjne. Czy to rywalizacja pomiędzy dużymi korporacjami, spory polityczne, czy nawet konfilkty zbrojne. Niestety, w takim wypadku, wszechobecnym stało się szerzenie dezinformacji, której skutki mogą być bardzo poważne. Z uwagi na powyższe, przyjmujemy, że korzystniejszym dla naszego klasyfikatora, będzie wychwytywanie dezinformacji (fake newsów) kosztem nieprawidłowego klasyfikowania informacji prawdziwych.

*Szybkie przypomnienie o rodzajach błędów:*
![Alt text](errors.jpg)

Tym sposobem, skupiamy się na minimalizowaniu **błędu typu II (fałszywie negatywny)**. W naszym projekcie, minimalizujemy wystąpienia przypadków, gdy zaklasyfikowaliśmy *fake news* jako prawdziwą informację. Zatem będziemy skupieni na czułości klasyfikatora, tj. naszą główną miarą będzie **recall**.


> ⚠️ Uwaga: Z uwagi na początkowe stadium projektu (oraz jak to bywa z tego rodzaju projektami), podejście do problemu może ulec zmianie w oparciu o uzyskane rezultaty. Dlatego zastrzegamy sobie prawo do zmiany strategii, modeli lub metryki.

Przygotowanie danych
=======================================================
Pierwsza myśl jaka może się nasuwać w przypadku formy tweetów jest ich niespójność. Hasztagi, akronimy, losowe znaki, literówki, linki itp. mogą przyspożyć wielu problemów. Weźmy przykładowy tweet z prawdziwą informacją:
```
"#RockyFire Update => California Hwy. 20 closed in both directions due to Lake County fire - #CAfire #wildfires"
```
Teraz zerknijmy na przykładowe tweety z nieprawdziwymi informacjami:
```
"Trident 90225 Chevy fire Truck w/ Pumper  IMS Fire Dept. Red HO 1:87  plastic http://t.co/FAQNFUpeGn http://t.co/mQfOfKXtyh"

"LOOOOOOL"
```
### *Jakie problemy dostrzegamy?*
------------------------
Wpier, skupiliśmy się na trzech podstawowych zbiegach czyszczenia tweetów. Są to:
- usunięcie znaków specjalnych, tkz. non-alphanumeric data + ogólne czyszczenie (tokenizing),
- usunięcie końcówek (afiksów) i zwracanie słowa pierwotnego (stemming, stem - łodyga),
- usunięcie słów nic nie wnoszących do tekstu, tkz. słów stop (stopwords).

## 1. Tokenizing
Token - ciąg znaków (często, ale nie zawsze, pojedynczych słów) w dokumencie,
gdzie dokument jest zbiorem tekstów, jak tweet, książka, papier itp.
 
Założenia:
- powinny być zawsze przechowywane w iterowalnej strukturze danych,
- wszystkie powinny mieć tę samą wielkość liter (małe/duże), aby zmniejszyć złożoność,
- powinny być wolne od znaków niealfanumerycznych.

Zatem, tworząc *tokeny* z przykładowych tweetów, z ciągów znaków (string) otrzymujemy listy słów, tj.:
```
tokenize("#RockyFire Update => California Hwy. 20 closed in both directions due to Lake County fire - #CAfire #wildfires")
    
['rockyfire',
'update',
'california',
'hwy',
'20',
'closed',
'in',
'both',
'directions',
'due',
'to',
'lake',
'county',
'fire',
'cafire',
'wildfires']
```
------------------------
    tokenize("Trident 90225 Chevy fire Truck w/ Pumper  IMS Fire Dept. Red HO 1:87  plastic http://t.co/FAQNFUpeGn http://t.co/mQfOfKXtyh")
```
['trident',
'90225',
'chevy',
'fire',
'truck',
'w',
'pumper',
'ims',
'fire',
'dept',
'red',
'ho',
'1',
'87',
'plastic',
'http',
't',
'co',
'faqnfupegn',
'http',
't',
'co',
'mqfofkxtyh']
```
------------------------
```
tokenize("LOOOOOOL")

['looooool']
```



## 2. Stemming
Stemming - usuwanie afiksów z wyrazów i zwracanie słowa głównego, np. rdzeń słowa 'working' zamienimy na 'work'. Co więcej, stemming nie bierze pod uwagę kontekstu w jakim wyrazy występują.

Do wydobycia "pierowtnej formy" ze słów, wykorzystamy algorytm Snowball Stemmer. Jako, że celem projektu jest model klasyfikacji, więc osoby pragnące *zanurzyć się* w algorytmie odysłam do [implementacji wraz z wyjaśnieniem](https://snowballstem.org/algorithms/porter/stemmer.html).

Zatem, wydobywając słowa pierwotne z naszych tweetów (w postaci *tokenów*), wykorzystując algorytm kuli śnieżnej, otrzymujemy:
```
clean_tweet = tokenize("#RockyFire Update => California Hwy. 20 closed in both directions due to Lake County fire - #CAfire #wildfires")
print([snowball.stem(w) for w in clean_tweet])
    
['rockyfir', 'updat', 'california', 'hwi', '20', 'close', 'in', 'both', 'direct', 'due', 'to', 'lake', 'counti', 'fire', 'cafir', 'wildfir']
```
------------------------
```
clean_tweet = tokenize("Trident 90225 Chevy fire Truck w/ Pumper  IMS Fire Dept. Red HO 1:87  plastic http://t.co/FAQNFUpeGn http://t.co/mQfOfKXtyh")
print([snowball.stem(w) for w in clean_tweet])

['trident', '90225', 'chevi', 'fire', 'truck', 'w', 'pumper', 'im', 'fire', 'dept', 'red', 'ho', '1', '87', 'plastic', 'http', 't', 'co', 'faqnfupegn', 'http', 't', 'co', 'mqfofkxtyh']
```
------------------------
```
clean_tweet = tokenize("LOOOOOOL")
print([snowball.stem(w) for w in clean_tweet])

['looooool']
```

## 3. Stopwords
Stopwords - pojęcie z zakresu SEO (search engine optimization - optymalizacja dla wyszukiwarek internetowych). Są to słowa, które nie wnoszą nic do tekstu z punktu widzenia logiki i przekazu oraz nie niosą za sobą żadnych istotnych treści, czyli najczęściej zaimki, przyimiki, liczebniki itp.

Usuwając słowa stop z naszych już przetworzonych tweetów, otrzymujemy:
```
clean_tweet = tokenize("#RockyFire Update => California Hwy. 20 closed in both directions due to Lake County fire - #CAfire #wildfires")
clean_tweet_primal = [snowball.stem(w) for w in clean_tweet]
print([w for w in clean_tweet_primal if w not in stop_words])

['rockyfir', 'updat', 'california', 'hwi', '20', 'close', 'direct', 'due', 'lake', 'counti', 'fire', 'cafir', 'wildfir']
```
------------------------
```
clean_tweet = tokenize("Trident 90225 Chevy fire Truck w/ Pumptęer  IMS Fire Dept. Red HO 1:87  plastic http://t.co/FAQNFUpeGn http://t.co/mQfOfKXtyh")
clean_tweet_primal = [snowball.stem(w) for w in clean_tweet]
print([w for w in clean_tweet_primal if w not in stop_words])

['trident', '90225', 'chevi', 'fire', 'truck', 'w', 'pumper', 'im', 'fire', 'dept', 'red', 'ho', '1', '87', 'plastic', 'http', 'co', 'faqnfupegn', 'http', 'co', 'mqfofkxtyh']
```
------------------------
```
clean_tweet = tokenize("LOOOOOOL")
clean_tweet_primal = [snowball.stem(w) for w in clean_tweet]
print([w for w in clean_tweet_primal if w not in stop_words])

['looooool']
```
## *Utworzenie słownika*
Dodakowo, przydatnym wydawałoby się utworzenie zbioru wszystkich zaobserwowanych słów/znaków, tj. słownik. Zatem, korzystając z poznanych metod obróbki, pokażmy na przykładzie wszystkich słów, ich skuteczność.

W przypadku naszych danych, w zbiorze treningowym występuje 7613 tweetów, każdy do 280 znaków. Co daje nam do **2.131.640** różnych słów/znaków w słowniku. Przystępując do działania:
```
Długość słownika wyniosła:
-> przed obróbką:                             129871
-> po tokenizacji (w tym usunięcie powieleń): 21571
-> po stemmingu:                              18540
-> po wykluczeniu stopwords:                  18413
```
Jak widać, redukcja ilości słów jest aż **7-krotna**, a więc znacząca.

------------------------
## Model danych
Następnym krokiem było utworzenie strukutry akceptowalnej dla klasyfikatora. Takową strukturą będzie tkz. **model bag of words** (worek słów).

![Alt text](bow.png)

*Bag of words* to reprezentacja tekstu, która opisuje występowanie słów w obrębie dokumentu. Obejmuje ona dwie rzeczy:

- Słownik wszystkich zaobserwowanych słów.
- Miarę obecności zaobserwowanych słów.

Jest on nazywany *"workiem"* słów, ponieważ wszelkie informacje o kolejności lub strukturze słów w dokumencie są **odrzucane**. Model jest zainteresowany **tylko** tym, czy znane słowa występują w dokumencie, a nie gdzie w dokumencie.

Model *bag of words* jest powszechnie stosowany w metodach klasyfikacji dokumentów, gdzie (częstotliwość) występowania każdego słowa jest wykorzystywana jako cecha do trenowania klasyfikatora. Dla przykładu, zbudujmy model worka słów w oparciu o recenzje filmowe:

```
# Recenzje filmów (positive/negative)
dataset = [["I liked the movie", "positive"],
           ["It’s a good movie. Nice story", "positive"],
           ["Hero’s acting is bad but heroine looks good. Overall nice movie", "positive"],
           ["Nice songs. But sadly boring ending.", "negative"],
           ["sad movie, boring movie", "negative"]]
```   
------------------------     
*Pamiętajmy o wcześniejszym przygotowaniu danych.*
```
dataset_clean = []
for _ in dataset.Text:
    tweet = _
    clean_tweet = tokenize(tweet)
    clean_tweet_primal = [snowball.stem(w) for w in clean_tweet]
    tweet_x = [w for w in clean_tweet_primal if w not in stop_words]
    dataset_clean.append(' '.join(tweet_x))
print(dataset_clean)

['like movi',
'good movi nice stori',
'hero act bad heroin look good overal nice movi',
'nice song sad bore end',
'sad movi bore movi']
```
------------------------  
*Wykorzystamy gotową funkcję z pakietu sklearn CountVectorizer().*
```
from sklearn.feature_extraction.text import CountVectorizer

cv = CountVectorizer(max_features = 1500)
X = cv.fit_transform(dataset_clean).toarray()
y = dataset.iloc[:, 1].values

bow = pd.DataFrame(X,columns=cv.get_feature_names_out())
print(bow)

   act  bad  bore  end  good  hero  heroin  like  look  movi  nice  overal   sad  song  stori 
0    0    0     0    0     0     0       0     1     0     1     0       0     0     0      0
1    0    0     0    0     1     0       0     0     0     1     1       0     0     0      1
2    1    1     0    0     1     1       1     0     1     1     1       1     0     0      0 
3    0    0     1    1     0     0       0     0     0     0     1       0     1     1      0 
4    0    0     1    0     0     0       0     0     0     2     0       0     1     0      0 
  
```

Sukces! Udało nam się uzyskać model bag of words. Zatem możemy przejść do implementacji klasyfikatora.

Klasyfikator Bayesowski
=======================================================
Metoda klasyfikatora Wielomianowego Naiwnego Bayesa jest szeroko stosowana do przypisywania dokumentów do klas na podstawie analizy statystycznej ich zawartości. Stanowi alternatywę dla "ciężkiej" analizy semantycznej opartej na AI i drastycznie upraszcza klasyfikację danych tekstowych.

Każdy dokument składa się z wielu słów, które przyczyniają się do zrozumienia treści dokumentu. Klasa jest znacznikiem jednego lub wielu dokumentów, odnoszącym się do tego samego tematu.

Zatem, rodzi się pytanie, czy jest to rozróżnialne? Czy można zliczyć słowa i obliczyć prwadopodobieństwo ich występowania w danej klasie? Zerknijmy na przykładowy histogram rozkładu mieszanego. Przyjmijmy, że **lewe wzniesienie** określa nam prawdopodobieństwo dla *fejk newsów*, natomiast **prawe wzniesienie** określa prawdopodobieństwo *rzetelnej informacji*.

![Alt text](norm_mix.png)

Idąc dalej, będziemy liczyć prawdopodobieństwo tego, czy dany tweet jest fejk newsem, w oparciu o zawartość jego słów.

$
\begin{align}
\mathsf{Pr}(fejk|tweet)\propto\mathsf{Pr}(fejk)\prod_{1\leq k\leq n_{tweet}}\mathsf{Pr}(word_k|fejk),
\end{align}
$

gdzie $n_{tweet}$ jest liczbą słów w danym tweecie, a $\mathsf{Pr}(word_k|fejk)$ jest prawdopodobieństwem warunkowym wystąpienia słowa $word_k$ w tweecie będącym *fejk newsem*. Dodatkowo, $\mathsf{Pr}(word_k|fejk)$ interpretujemy jako miarę tego jak prawdopodobnym jest, że tweet jest *"fejkiem"*. $\mathsf{Pr}(fejk)$ jest prawdopodobieństwem a'priori wystąpienia słów w *fejk newsie*.

Należy również dodać, że **zakładamy niezależne występowanie słów** w danym tweecie. Stąd też, wykorzystanie produktu we wzorze klasyfikatora, ponieważ mnożymy przez siebie kolejne napotkane słowa.

W klasyfikacji tekstu, naszym celem jest znalezienie najlepszej klasy dla danego dokumentu (tweeta). W tym celu wykorzystamy metodę dopasowania rozkładu maximum a posteriori (MAP):

$
\begin{align}
fejk_{map}=\argmax_{fejk \in \{0,1\}}\hat{\mathsf{Pr}}(fejk|tweet)=\argmax_{fejk \in \{0,1\}}\hat{\mathsf{Pr}}(fejk)\prod_{1\leq k\leq n_{tweet}}\hat{\mathsf{Pr}}(word_k|fejk).
\end{align}
$

Zapisujemy $\hat{\mathsf{Pr}}$ dla $\mathsf{Pr}$ ponieważ nie znamy prawdziwych wartości parametrów $\mathsf{Pr}(fejk)$ oraz $\mathsf{Pr}(word_k|fejk)$.

Dla osób ciekawych matematyki stojącą za klasyfikatorem odsyłam do [The Stanford Natural Language Processing Group](https://nlp.stanford.edu/IR-book/html/htmledition/naive-bayes-text-classification-1.html), którzy to, przystępnie omawiają zaproponowaną metodę.

Pseudokod powyższej metody jest nastepujący:

![Alt text](mNB_pseudocode.png)

## Implementacja
W celu implementacji wykorzystamy [gotową funckję](https://scikit-learn.org/stable/modules/generated/sklearn.naive_bayes.MultinomialNB.html) z pakietu scikit-learn (1.1.3), która została napisana w oparciu pracę zespoły [The Stanford Natural Language Processing Group](https://nlp.stanford.edu/IR-book/html/htmledition/naive-bayes-text-classification-1.html).

------------------------ 
*Implementacji metody na naszym zbiorze tweetów.*
### Załadowanie i wstępne oczyszczenie danych
```
Dataset = pd.read_csv("Dataset.csv", delimiter = ',')

# Removing '%20' from the middle and 's' from the end of keywords
# (removing 's' reduced set only by 7 keywords...)
Dataset.keyword = Dataset.keyword.str.replace(r'%20', ' ').str.rstrip('s')

# Check for any of characters not being alphanumeric
Dataset.assign(special = lambda x: x['keyword'].str.replace(' ', '').str.isalnum() == False)\
    .special.value_counts()  # no more nonalphanumeric values in keywords

# Switch y to reverse, ie. 1 - fake news; 0 - real news
Dataset.target.value_counts()
# 0    4342
# 1    3271
# Name: target, dtype: int64

Dataset.target = operator.xor(Dataset.target,1)
Dataset.target.value_counts()
# 1    4342
# 0    3271
# Name: target, dtype: int64

Dataset
```
![Alt text](Dataset.png)

### Utworzenie słownika
```
# tokenizing function with removing all-nonalphanumeric data
def tokenize(text):
    # set as low
    text = text.lower()
    # remove all non-alphanumeric characters
    text = re.sub(r'[^a-zA-Z 0-9]', ' ', str(text)) # przechodzac (od lewej) po ciagu znakow, te które nie zostały wskazane w 'pattern' zamienia na wskazana wartosc 'repl'
    # return iterable list of characters
    return text.split()

dictionary = []
for tweets in Dataset.text:
    # usuniecie nie alfphanumeric fraz ++ 
    tweet = tokenize(tweets)
    for word in tweet:
        dictionary.append(word)
set_dictionary = set(dictionary)
dictionary = list(set_dictionary)
```

```
# usuniecie koncowek 
snowball = SnowballStemmer('english')
dictionary = [snowball.stem(w) for w in dictionary]
set_dictionary = set(dictionary)
dictionary = list(set_dictionary)

# usuniecie slow nic nie wnoszacych do tekstu
stop_words = set(stopwords.words('english'))
dictionary = [w for w in dictionary if w not in stop_words]
set_dictionary = set(dictionary)
dictionary = list(set_dictionary)
dictionary.sort()

print(dictionary)

['0', '00', '000', '0000', '007npen6lg', ... , 'zztbvjypn1', 'zzweeezjug', 'zzzz']
```

### Model bag of words
```
# utworzenie formy "podajnika" dla wektora slow
df = pd.DataFrame(dictionary, columns = ['words'])
df['counts'] = 0
dictionary_wide = pd.pivot(df, columns = 'words', values = 'counts').head(1).fillna(0)
dictionary_wide.shape

(1, 18413)
```

```
# wypelnienie "podajnika"
X = []
for i in range(len(Dataset)):
    
    # oczyszczenie tweeta
    tweet = tokenize(Dataset.text[i])
    tweet_sb = [snowball.stem(w) for w in tweet]
    tweet_sw = [w for w in tweet_sb if w not in stop_words]
    
    # forma
    document = copy.deepcopy(dictionary_wide)
    
    # zliczenie slow
    for _ in tweet_sw:
        document[_] += 1
    
    # dodanie do x-ow
    X.append(document.values[0])
    
X = np.array(X)
y = np.array(Dataset.target)
```

```
X

array([[0., 0., 0., ..., 0., 0., 0.],
       [0., 0., 0., ..., 0., 0., 0.],
       [0., 0., 0., ..., 0., 0., 0.],
       ...,
       [0., 0., 0., ..., 0., 0., 0.],
       [0., 0., 0., ..., 0., 0., 0.],
       [0., 0., 0., ..., 0., 0., 0.]])
```

```
y

array([0, 0, 0, ..., 0, 0, 0], dtype=int64)
```

### Podział na zbiór treningowy i testowy
```
X_train, X_test, y_train, y_test = train_test_split(
           X, y, test_size = 0.25, random_state = 1)
```

### Utworzenie klasyfikatora
```
mNB = MultinomialNB()
mNB.fit(X_train, y_train)

y_pred = mNB.predict(X_test)
```

### Rezultat
```
cm = confusion_matrix(y_test, y_pred)

sns.heatmap(cm.T, square=True, annot=True, fmt='d', cbar=False)
plt.xlabel('true label')
plt.ylabel('predicted label')
```
![Alt text](heatmap.png)

```
print("Classification report:\n",classification_report(y_test, y_pred))

# Classification report:
#                precision    recall  f1-score   support
#
#            0       0.78      0.72      0.75       797
#            1       0.81      0.85      0.83      1107
#
#     accuracy                           0.80      1904
#    macro avg       0.79      0.79      0.79      1904
# weighted avg       0.80      0.80      0.80      1904
```
Recall dla fake newsów (w przypadku powtarzanych, ponownych losowań podziału na zbior treningowy i testowy) w okolicach 85%. Znacznie powyżej 50% (rzut monetą).

------------------------ 
#TODO: 
- Rozwinąć model o metodę [TFIDF](https://pl.wikipedia.org/wiki/TFIDF) (funkcja TfidfVectorizer()) - gotowe,
- pokazać jak model klasyfikuje wybrane tweety - gotowe,
- dodać pełną wizualzację przebiegu modelu dla jednego tweeta - mało sensowne, ale jest pokazany rozkład Pr() dla słów tweeta.

*(Dominik)*
=======================================================