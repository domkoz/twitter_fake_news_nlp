**CEL ZADANIA**

Fanpage facebookowy „Konflikty i Katastrofy Światowe – KIKŚ” chce zautomatyzować proces zdobywania informacji
o pojawiających się na świecie problemach. Jeden z administratorów strony zauważył, że świetnym źródłem najnowszych
wiadomości są wiadomości twitterowe – w końcu jak długo zajmuje napisanie 160 znaków o tym, że gdzieś wystąpiła
sytuacja kryzysowa? Pojawił się jednak pewien problem – nie zawsze bowiem coś, co postujący uznał za katastrofę
faktycznie nią jest. Co więcej, część informacji celowo była fałszowana, by wywołać panikę lub w innych, niecnych celach.
 Dysponując zbiorem danych zawierających twitty o przeszłych katastrofach oraz te zawierające nieprawdziwe informacje,
Twoim zadaniem jest stworzenie klasyfikatora, który będzie poprawnie rozróżniał fake newsy od faktycznych informacji
o katastrofach. Administrator strony marzy również o generatorze podsumowań, który będzie tworzył krótką notatkę
prasową na podstawie kilku twittów… ale to już może okazać się poza spektrum tego projektu.

**Opis zbioru danych**

Zarówno zbiór testowy jak i treningowy posiadają następujące kolumny:

*id* – identyfikator liczbowy danego twitta;

*text* – treść twitta;

*location* – lokalizacja, z której twitt został wysłany (puste w przypadku gdy wysyłający nie miał udostępnionej lokalizacji w telefonie lub komputerze);

*keyword* – wyróżnione słowa kluczowe (mogą być puste);

*target* – w zbiorze treningowym, zmienna logiczna opisująca prawdziwość przekazywanej informacji (1), lub (0) dla fake newsów i informacji nieistotnych.