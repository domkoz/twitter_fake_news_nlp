# -*- coding: utf-8 -*-
"""
Created on Sat Nov 12 18:23:46 2022

@author: Bartosz
"""
# %% libraries ========================================================================================
import pandas as pd
import numpy as np
import re
from nltk.stem import SnowballStemmer
from nltk.corpus import stopwords
from sklearn.model_selection import train_test_split
import copy
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import confusion_matrix, classification_report
import operator
import matplotlib.pyplot as plt
import seaborn as sns
import math
from sklearn.feature_extraction.text import TfidfVectorizer

# from datetime import datetime
# %% przygotowanie danych ========================================================================================
# Wszystko zgodne mimo problemów w samym pliku csv :)
Dataset = pd.read_csv("Dataset.csv", delimiter=",")

# Removing '%20' from the middle and 's' from the end of keywords
# (removing 's' reduced set only by 7 keywords...)
Dataset.keyword = Dataset.keyword.str.replace(r"%20", " ").str.rstrip("s")

# Check for any of characters not being alphanumeric
Dataset.assign(
    special=lambda x: x["keyword"].str.replace(" ", "").str.isalnum() == False
).special.value_counts()  # no more nonalphanumeric values in keywords

# Switch y to reverse, ie. 1 - fake news; 0 - real news
Dataset.target.value_counts()
# 0    4342
# 1    3271
# Name: target, dtype: int64
Dataset.target = operator.xor(Dataset.target, 1)
Dataset.target.value_counts()
# 1    4342
# 0    3271
# Name: target, dtype: int64

# Dataset.drop_duplicates(subset=['text'], keep=False, inplace=True)

Dataset
# =============================================================================
# id – identyfikator liczbowy danego twitta;
# text – treść twitta;
# location – lokalizacja, z której twitt został wysłany (puste w przypadku,
# gdy wysyłający nie miał udostępnionej lokalizacji w telefonie lub komputerze);
# keyword – wyróżnione słowa kluczowe (mogą być puste);
# target – w zbiorze treningowym, zmienna logiczna opisująca prawdziwość
# przekazywanej informacji (1), lub (0) dla fake newsów i informacji nieistotnych.
# =============================================================================

# pomysl:
#    tokenize (usuniecie nie alfphanumeric fraz) ->
#    Stemming/Lemmatization (usuniecie koncowek/wybranie odpowiednich koncowek) ->
#    stopwords (usuniecie slow nic nie wnoszacych do tekstu)

# nastepnie sprobowac wydzielic slowa, ktore wygladaja jak linki
# oraz liczby wydzielic jako liczby
# usuniecie literowek poprzez algorytm wyszukujacy wyrazen podobnych
# moze jeszcze dodac autorow do slownika ?

# Jak zabraknie pomyslow, to ktos juz tweety obrabial:
# https://towardsdatascience.com/pre-processing-should-extract-context-specific-features-4d01f6669a7e
# %% utworzenie słownika ========================================================================================

# tokenizing function with removing all-nonalphanumeric data
def tokenize(text):
    # set as low
    text = text.lower()
    # remove all non-alphanumeric characters
    text = re.sub(
        r"[^a-zA-Z 0-9]", " ", str(text)
    )  # przechodzac (od lewej) po ciagu znakow, te które nie zostały wskazane w 'pattern' zamienia na wskazana wartosc 'repl'
    # return iterable list of characters
    return text.split()


Dataset.iloc[40, 3]
tokenize(Dataset.iloc[40, 3])

dictionary = []
for tweets in Dataset.text:
    # usuniecie nie alfphanumeric fraz ++
    tweet = tokenize(tweets)
    for word in tweet:
        dictionary.append(word)
print("Dlugosc slownika:")
print("-> przed usuwaniem powielen:   ", len(dictionary))
set_dictionary = set(dictionary)
dictionary = list(set_dictionary)
print("-> po usunieciu powielen:      ", len(dictionary))
# usuniecie koncowek
snowball = SnowballStemmer("english")
dictionary = [snowball.stem(w) for w in dictionary]
set_dictionary = set(dictionary)
dictionary = list(set_dictionary)
print("-> po uspojnieniu koncowek:    ", len(dictionary))
# usuniecie slow nic nie wnoszacych do tekstu
stop_words = set(stopwords.words("english"))
dictionary = [w for w in dictionary if w not in stop_words]
set_dictionary = set(dictionary)
dictionary = list(set_dictionary)
print("-> po usunieciu zbednych slow: ", len(dictionary))
dictionary.sort()

# tst = np.empty([0,0], dtype = '<U44')
# tst = np.insert(tst, len(tst), 'tst')
# tst
# dictionary = np.empty([0], dtype = '<U44')
# for tweet in Dataset.text:
#     dictionary = np.insert(dictionary, len(dictionary), tokenize(tweet))
# dictionary = set(dictionary)

# %% BOW model (bag of words)
# utworzenie formy "podajnika" dla wektora slow
df = pd.DataFrame(dictionary, columns=["words"])
df["counts"] = 0
dictionary_wide = pd.pivot(df, columns="words", values="counts").head(1).fillna(0)
dictionary_wide.shape


# wypelnienie "podajnika"
X = []
for i in range(len(Dataset)):

    # oczyszczenie tweeta
    tweet = tokenize(Dataset.text[i])
    tweet_sb = [snowball.stem(w) for w in tweet]
    tweet_sw = [w for w in tweet_sb if w not in stop_words]

    # forma
    document = copy.deepcopy(dictionary_wide)

    # zliczenie slow
    for _ in tweet_sw:
        document[_] += 1

    # dodanie do x-ow
    X.append(document.values[0])

X = np.array(X)
y = np.array(Dataset.target)

# # Dataset.text[0]
# # tst = pd.melt(document).sort_values('value', ascending = False)
# X[0][np.newaxis, ...]
# y[0][np.newaxis, ...]

# %% Naive Bayes Classifier

X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.25, random_state=1
)

mNB = MultinomialNB()
mNB.fit(X_train, y_train)

# predicting test set results
y_pred = mNB.predict(X_test)  # Dalsze plany: TfidfVectorizer()

# %% Visualization
# making the confusion matrix
cm = confusion_matrix(y_test, y_pred)
# print("Confusion Matrix:\n",cm)
# Confusion Matrix:
#  [[575 222]
#  [166 941]]

ax = plt.subplot()
sns.heatmap(cm, square=True, annot=True, fmt="d", cbar=False)
plt.xlabel("true label")
plt.ylabel("predicted label")
ax.xaxis.set_ticklabels(["real_news", "fake_news"])
ax.yaxis.set_ticklabels(["real_news", "fake_news"])

print("Classification report:\n", classification_report(y_test, y_pred))
# Classification report:
#                precision    recall  f1-score   support
#
#            0       0.78      0.72      0.75       797
#            1       0.81      0.85      0.83      1107
#
#     accuracy                           0.80      1904
#    macro avg       0.79      0.79      0.79      1904
# weighted avg       0.80      0.80      0.80      1904
# Recall dla fake newsów w okolicach 85%. Znacznie powyżej 50% (rzut monetą). Fajny wynik.

# %% Visualization based on 1 tweet

tweet = "#RockyFire Update => California Hwy. 20 closed in both directions due to Lake County fire - #CAfire #wildfires"
print(
    "====================================================================================================================",
    "\n",
    tweet,
    "\n",
    "====================================================================================================================",
)

# Kto chcę napisać tweeta? :D (należy pamiętać, że słownik jest ograniczony w słowa)
# tweet = "Prince want to sent you some fat money"
clean_tweet = tokenize(tweet)
clean_tweet_primal = [snowball.stem(w) for w in clean_tweet]
tweet_x = [w for w in clean_tweet_primal if w not in stop_words]

# Utworzenie formy
doc = copy.deepcopy(dictionary_wide)
for _ in tweet_x:
    doc[_] += 1
# indeksy z wartoscia niezerową (występowanie słów)
print("Indeksy słów w słowniku:\n", np.where(doc > 0)[1])

print("Słownik:\n", doc.iloc[:, np.where(doc > 0)[1]])

# slownik prawdopodobienstw
doc_prob0 = copy.deepcopy(dictionary_wide)
doc_prob1 = copy.deepcopy(dictionary_wide)
for _ in tweet_x:
    doc_prob0[_] += mNB.feature_log_prob_[0][doc_prob0.columns.get_loc(_)]
    doc_prob1[_] += mNB.feature_log_prob_[1][doc_prob1.columns.get_loc(_)]

prob = pd.DataFrame(
    {
        "fake news": doc_prob0.iloc[:, np.where(doc_prob0 != 0)[1]].apply(math.exp),
        "real news": doc_prob1.iloc[:, np.where(doc_prob1 != 0)[1]].apply(math.exp),
    }
)

print("Prawdopodobienstwo występownia poszczególnych słów w podziale na klasy:\n", prob)

# policzmy prawdopodobienstwo automatycznie
# print(mNB.classes_) # liczba klas w modelu
# print(mNB.class_log_prior_) # wyliczone prawdopodobienstwo a priori dla klas
# print(mNB.feature_log_prob_) # wyliczone prawdopodobienstwo a priori dla slow


def manual_fit(ind: list):

    real = mNB.classes_[0]
    real_news_prob = mNB.class_log_prior_[real]
    for _ in ind:
        real_news_prob += mNB.feature_log_prob_[real][_]

    fake = mNB.classes_[1]
    fake_news_prob = mNB.class_log_prior_[fake]
    for _ in ind:
        fake_news_prob += mNB.feature_log_prob_[fake][_]

    return math.exp(real_news_prob), math.exp(fake_news_prob)


# policzone manualnie
print(
    "Pr(real)=",
    f"{manual_fit(np.where(doc > 0)[1])[0]:.100f}",
    "\nPr(fake)=",
    f"{manual_fit(np.where(doc > 0)[1])[1]:.100f}",
)
print("Ostatecznie -> ", np.argmax(manual_fit(np.where(doc > 0)[1])))

# policzone modelem
print("Sprawdzenie z gotowa funkcja:", mNB.predict(np.array(doc)))


# %% TF-IDF (automate cleaning)
# https://www.learndatasci.com/glossary/tf-idf-term-frequency-inverse-document-frequency/

tweets_tfidf = np.array(Dataset.text)
target_tfidf = np.array(Dataset.target)

X_train, X_test, y_train, y_test_tf = train_test_split(
    tweets_tfidf, target_tfidf, test_size=0.25, random_state=1
)

vectorizer = TfidfVectorizer()
X_train_tf = vectorizer.fit_transform(X_train)

X_test_tf = vectorizer.transform(X_test)

mNB.fit(X_train_tf, y_train)

y_pred_tf = mNB.predict(X_test_tf)

# %% Visualization TF-IDF
# making the confusion matrix
cm_tf = confusion_matrix(y_test_tf, y_pred_tf)
# print("Confusion Matrix:\n",cm_tf)
# Confusion Matrix:
# [[ 501  312]
#  [  78 1013]]

sns.heatmap(cm_tf, square=True, annot=True, fmt="d", cbar=False)
plt.xlabel("true label")
plt.ylabel("predicted label")

print(
    "Classification report (before cleaning):\n",
    classification_report(y_test_tf, y_pred_tf),
)
# Classification report (before cleaning):
#                precision    recall  f1-score   support

#            0       0.87      0.62      0.72       813
#            1       0.76      0.93      0.84      1091

#     accuracy                           0.80      1904
#    macro avg       0.81      0.77      0.78      1904
# weighted avg       0.81      0.80      0.79      1904
# Recall dla fake newsów w okolicach 93%. Poprawiony wynik poprzedniego modelu.
# Finalnie jednak, decyzja wyniku pozostaje dla klienta (tracimy na recall dla prawdziwych newsow)


# %% Brudnopis
# import nltk
# from nltk.corpus import stopwords
# from nltk.stem.porter import PorterStemmer
# from sklearn.feature_extraction.text import CountVectorizer
# from sklearn.naive_bayes import GaussianNB

dataset = [
    ["I liked the movie", "positive"],
    ["It’s a good movie. Nice story", "positive"],
    [
        "Hero’s acting is bad but heroine looks good.\
            Overall nice movie",
        "positive",
    ],
    ["Nice songs. But sadly boring ending.", "negative"],
    ["sad movie, boring movie", "negative"],
]

dataset = pd.DataFrame(dataset)
dataset.columns = ["Text", "Reviews"]

nltk.download("stopwords")

corpus = []

for i in range(0, 5):
    text = re.sub("[^a-zA-Z]", " ", dataset["Text"][i])
    text = text.lower()
    text = text.split()
    ps = PorterStemmer()
    text = " ".join(text)
    corpus.append(text)
print("corpus:\n", corpus, "\n-----------------------")

# creating bag of words model
cv = CountVectorizer(max_features=1500)

X = cv.fit_transform(corpus).toarray()
y = dataset.iloc[:, 1].values

print("X:\n", X, "\n-----------------------")
print("feature_names:\n", cv.get_feature_names_out(), "\n-----------------------")

# splitting the data set into training set and test set
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.25, random_state=0
)

# fitting naive bayes to the training set
classifier = GaussianNB()
classifier.fit(X_train, y_train)

# %% TF-IDF + cleaning
# https://www.learndatasci.com/glossary/tf-idf-term-frequency-inverse-document-frequency/

# cleaning
X = []
df = Dataset.text.apply(lambda x: tokenize(x))
for tweet in df:

    # oczyszczenie tweeta
    tweet_sb = [snowball.stem(w) for w in tweet]
    tweet_sw = [w for w in tweet_sb if w not in stop_words]
    tweet_clean = " ".join(tweet_sw)

    # dodanie do x-ow
    X.append(tweet_clean)

X_train, X_test, y_train, y_test_tf = train_test_split(
    np.array(X), np.array(Dataset.target), test_size=0.25, random_state=1
)

vectorizer = TfidfVectorizer()
X_train_tf_clean = vectorizer.fit_transform(X_train)

X_test_tf_clean = vectorizer.transform(X_test)

mNB.fit(X_train_tf_clean, y_train)

y_pred_tf_clean = mNB.predict(X_test_tf_clean)

# %% Visualization TF-IDF clean
# making the confusion matrix
cm_tf = confusion_matrix(y_test_tf, y_pred_tf)
# print("Confusion Matrix:\n",cm_tf)
# Confusion Matrix:
# [[ 501  312]
#  [  78 1013]]

sns.heatmap(cm_tf, square=True, annot=True, fmt="d", cbar=False)
plt.xlabel("true label")
plt.ylabel("predicted label")

print(
    "Classification report (after cleaning):\n",
    classification_report(y_test_tf, y_pred_tf),
)
# Classification report (after cleaning):
#                precision    recall  f1-score   support

#            0       0.87      0.62      0.72       813
#            1       0.76      0.93      0.84      1091

#     accuracy                           0.80      1904
#    macro avg       0.81      0.77      0.78      1904
# weighted avg       0.81      0.80      0.79      1904
# Recall dla fake newsów w okolicach 93%. Poprawiony wynik poprzedniego modelu.
# Cleaning nie wpłynłą (doczytać, czy to się dzieje automatycznie)


# predicting test set results
y_pred = classifier.predict(X_test)

# making the confusion matrix
cm = confusion_matrix(y_test, y_pred)
print("Confusion Matrix:\n", cm)

# %%
