# -*- coding: utf-8 -*-
"""
Created on Sat Oct 29 22:43:46 2022

@author: Bartosz
https://towardsdatascience.com/implement-expectation-maximization-em-algorithm-in-python-from-scratch-f1278d1b9137
"""
# %% libraries ========================================================================================
import pandas as pd
import numpy as np
import math
from scipy import stats, special
import matplotlib.pyplot as plt
import re
from nltk.stem import SnowballStemmer
from nltk.corpus import stopwords
from sklearn.model_selection import train_test_split
import copy
from datetime import datetime
# %% parametry
# bezpośrednie oszacowania parametrów, gdy znane są etykiety (wzory jawne)

def learn_params(x_labeled, y_labeled):
    n = x_labeled.shape[0]
    
    phi = x_labeled[y_labeled == 1].shape[0] / n
    
    mu_0 = np.sum(x_labeled[y_labeled == 0], axis = 0) / x_labeled[y_labeled == 0].shape[0]
    mu_1 = np.sum(x_labeled[y_labeled == 1], axis = 0) / x_labeled[y_labeled == 1].shape[0]
    
    sigma_0 = np.cov(x_labeled[y_labeled == 0].T, bias=True)
    sigma_1 = np.cov(x_labeled[y_labeled == 1].T, bias=True)
    
    params = {'phi': phi,
              'mu_0': mu_0,
              'mu_1': mu_1,
              'sigma_0': sigma_0,
              'sigma_1': sigma_1}
    
    return params

# %% Expectation step - korzystamy z Twierdzenia Bayesa (mamy przeciez rozklad a posteriori) ======================

# tworzymy algorytm dla klasyfikacji binarnej (y = 1 lub y = 0) - stąd bierzemy dwa rozklady
def e_step(x, params):
    
    # wykorzystamy wbudowana funkcje do rozkladow wielomwymiarowych gaussowiskich z paczki stats
    # dodatkowo wykorzystamy funkcje, ktora "wyciaga" funkcje gestosci z rozkladu - pdf(x)   
    # dzieki temu, nie bedziemy musieli pisac tych funkcji recznie, a skupimy sie na samym algorytmie
    
    # utworzenie prawdopodobienstw dla kazdego punktu, tj.:
    # rozklad x-a w przypadku y=0 (wstepine, jeszcze nie ma y-kow)
    pdf_y0 = stats.multivariate_normal(params['mu_0'], params['sigma_0']).pdf(x)
    # rozklad x-a w przypadku y=1 (wstepine, jeszcze nie ma y-kow)
    pdf_y1 = stats.multivariate_normal(params['mu_1'], params['sigma_1']).pdf(x)
    
    # logarytm tych dwoch wartosci (dzieki liscie jest obliczany iteracyjnie)
    # otrzymujemy dwie listy z wartosciami log - [[log(x_1|y0), ...],[log(x_1|y1), ...]]
    log_p_x = np.log([pdf_y0, pdf_y1])
    
    # dodajemy do siebie obliczone prawdopodobienstwo punkotw jednej oraz drugiej klasy
    # + wylosowane phi (wylosowane prawdopodobienstwo danej klasy)
    # dlaczego dodajemy? poniewaz mamy logarytmy, tj.
    # nie mnozymy prawdopodobienstw p(x|y0)*p(y0), gdzie p(y0) okresla phi, tylko mozemy dodac
    # [np.newaxis, ...] - dodaje wymiar zadanej liscie
    # https://stackoverflow.com/questions/29241056/how-do-i-use-np-newaxis
    # np.array([[1,2,3]]).shape
    # (1, 3) - 1 wiersz i 3 kolumny
    # np.array([1,2,3]).shape
    # (3,) - lista, wiec ma inny zapis (niemacierzowy)
    # np.array([1,2,3])[np.newaxis, ...].shape - dodanie jako wiersza, tj. zamiana w macierz
    # (1, 3)
    # np.array([1,2,3])[..., np.newaxis].shape - dodanie jako kolumn
    # (3, 1)
    # w ten sposob uzyskujemy liczniki do tw. Bayesa
    log_p_y_x = np.log([1 - params['phi'], params['phi']])[np.newaxis, ...] + log_p_x.T
    
    # sumujemy obie klasy - mianownik do twierdzenia bayesa
    # scipy.special.logsumexp - Compute the log of the sum of exponentials of input elements.
    log_p_y_x_norm = special.logsumexp(log_p_y_x, axis=1)
    
    # zwracamy mianownik (dzieki funkcji logsumexp() mamy odwrocenie logarytmowania
    # stad otrzymujemy calkowity rozklad prawdopodobienstwa
    # oraz prawdopodobienstwa poszczegolnych punkotw, bo odejmowanie bedzie dzialc jak dzielnie
    # tj log(p(x|y0)*p(y0)/p(x)) => p(x|y0) + p(y0) - p(x)
    # gdzie p(x|y0)*p(y0) = log_p_y_x; p(x) = log_p_y_x_norm
    return log_p_y_x_norm, np.exp(log_p_y_x - log_p_y_x_norm[..., np.newaxis])

# %% Maximization step - korzystamy ze wzorów jawnych na parametry thety =========================================
# Uwaga! Jest to możliwe tylko w przypadku danych jakosciowych (labelki). W przeciwnym wypadku, trzeba by było liczyć pchodne/gradient.
# Wzory jawne powstają z dokładnych parametrów algorytmu EM. W rzeczywistości jedyną różnicą jest to,
# że rozwiązania EM używają heurystyki posteriors rozkładu, podczas gdy bezpośrednie oszacowania używają prawdziwych etykiet.

def m_step(x, params):
    total_count = x.shape[0]
    _, heuristics = e_step(x, params)
    
    heuristic0 = heuristics[:, 0] # prawdopodobienstwa klasy 0
    heuristic1 = heuristics[:, 1] # prawdopodobienstwa klasy 1
    
    sum_heuristic0 = sum(heuristic0)
    sum_heuristic1 = sum(heuristic1)
    
    phi = (sum_heuristic1/total_count)
    
    # trzeba "rozplaszczyc" heuristics0 do postaci macierzy (1,1000) (lista w liscie)
    # nastepenie iloczyn skalarny heurystyki z wartosciami x1 i x2
    # na koniec "rozplaszczamy" flatten()-em do listy wartosci
    mu_0 = (sum(heuristic0[np.newaxis,...].dot(x))/sum_heuristic0).flatten()
    mu_1 = (sum(heuristic1[np.newaxis,...].dot(x))/sum_heuristic1).flatten()
    
    diff0 = x - mu_0
    sigma_0 = diff0.T.dot(diff0 * heuristic0[..., np.newaxis]) / sum_heuristic0
    
    diff1 = x - mu_1
    sigma_1 = diff1.T.dot(diff1 * heuristic1[..., np.newaxis]) / sum_heuristic1
    
    params = {'phi': phi,
              'mu_0': mu_0,
              'mu_1': mu_1,
              'sigma_0': sigma_0,
              'sigma_1': sigma_1}
    
    return params

# %% Algorytm EM ==================================================================================================

def get_avg_log_likelihood(x, params):
    loglikelihood, _ = e_step(x, params)
    return np.mean(loglikelihood) # zwraca srednie dopasowanie z rozkladu

def run_EM(x, params, alpha, limit):
    avg_loglikelihoods = []
    i = 0
    while True:
        print(datetime.now().strftime("%H:%M:%S"))
        i += 1
        print("------------------ Iteracja:",i,"------------------")
        avg_loglikelihood = get_avg_log_likelihood(x, params)
        avg_loglikelihoods.append(avg_loglikelihood)
        
        # jezeli roznica miedzy ostatnim, a przedostatnim jest mniejsza niz alpha, przerwij cuzenie
        if ((len(avg_loglikelihoods) > 2 and abs(avg_loglikelihoods[-1] - avg_loglikelihoods[-2]) < alpha) or (i >= limit)):
            break
        
        # w przeciwnym wypadku, ucz dalej (podmiana starych parametrow na nowe)
        params = m_step(x, params)
        
    print("\tphi: %s\n\tmu_0: %s\n\tmu_1: %s\n\tsigma_0: %s\n\tsigma_1: %s"
               % (params['phi'], params['mu_0'], params['mu_1'], params['sigma_0'], params['sigma_1']))
    
    # wyliczamy wartosc oczekiwana przy wyuczonych parametrach
    _, posterior = e_step(x, params)
    
    # wykorzystujac np.argmax(), iteracyjnie, wybieramy klase z wiekszym prawdopodobienstwem
    forecasts = np.argmax(posterior, axis=1)
    
    return forecasts, posterior, avg_loglikelihoods

# %% przygotowanie danych ========================================================================================
# Wszystko zgodne mimo problemów w samym pliku csv :)
Dataset = pd.read_csv("Dataset.csv", delimiter = ',')

# Removing '%20' from the middle and 's' from the end of keywords
# (removing 's' reduced set only by 7 keywords...)
Dataset.keyword = Dataset.keyword.str.replace(r'%20', ' ').str.rstrip('s')

# Check for any of characters not being alphanumeric
Dataset.assign(special = lambda x: x['keyword'].str.replace(' ', '').str.isalnum() == False)\
    .special.value_counts()  # no more nonalphanumeric values in keywords

Dataset
# =============================================================================
# id – identyfikator liczbowy danego twitta;
# text – treść twitta;
# location – lokalizacja, z której twitt został wysłany (puste w przypadku,
# gdy wysyłający nie miał udostępnionej lokalizacji w telefonie lub komputerze);
# keyword – wyróżnione słowa kluczowe (mogą być puste);
# target – w zbiorze treningowym, zmienna logiczna opisująca prawdziwość
# przekazywanej informacji (1), lub (0) dla fake newsów i informacji nieistotnych.
# =============================================================================

# pomysl:
#    tokenize (usuniecie nie alfphanumeric fraz) ->
#    Stemming/Lemmatization (usuniecie koncowek/wybranie odpowiednich koncowek) ->
#    stopwords (usuniecie slow nic nie wnoszacych do tekstu)

# nastepnie sprobowac wydzielic slowa, ktore wygladaja jak linki
# oraz liczby wydzielic jako liczby
# usuniecie literowek poprzez algorytm wyszukujacy wyrazen podobnych
# moze jeszcze dodac autorow do slownika ?

# Jak zabraknie pomyslow, to ktos juz tweety obrabial:
# https://towardsdatascience.com/pre-processing-should-extract-context-specific-features-4d01f6669a7e
# %% utworzenie słownika ========================================================================================

# tokenizing function with removing all-nonalphanumeric data
def tokenize(text):
    # set as low
    text = text.lower()
    # remove all non-alphanumeric characters
    text = re.sub(r'[^a-zA-Z 0-9]', ' ', str(text)) # przechodzac (od lewej) po ciagu znakow, te które nie zostały wskazane w 'pattern' zamienia na wskazana wartosc 'repl'
    # return iterable list of characters
    return text.split()

Dataset.iloc[40,3]
tokenize(Dataset.iloc[40,3])

dictionary = []
for tweets in Dataset.text:
    # usuniecie nie alfphanumeric fraz ++ 
    tweet = tokenize(tweets)
    for word in tweet:
        dictionary.append(word)
print("Dlugosc slownika:")
print("-> przed usuwaniem powielen:   ",len(dictionary))
set_dictionary = set(dictionary)
dictionary = list(set_dictionary)
print("-> po usunieciu powielen:      ",len(dictionary))
# usuniecie koncowek 
snowball = SnowballStemmer('english')
dictionary = [snowball.stem(w) for w in dictionary]
set_dictionary = set(dictionary)
dictionary = list(set_dictionary)
print("-> po uspojnieniu koncowek:    ",len(dictionary))
# usuniecie slow nic nie wnoszacych do tekstu
stop_words = set(stopwords.words('english'))
dictionary = [w for w in dictionary if w not in stop_words]
set_dictionary = set(dictionary)
dictionary = list(set_dictionary)
print("-> po usunieciu zbednych slow: ",len(dictionary))
dictionary.sort()

# tst = np.empty([0,0], dtype = '<U44')
# tst = np.insert(tst, len(tst), 'tst')
# tst
# dictionary = np.empty([0], dtype = '<U44')
# for tweet in Dataset.text:
#     dictionary = np.insert(dictionary, len(dictionary), tokenize(tweet))
# dictionary = set(dictionary)

# %% utworzenie zbioru testowego ==================================================================================

# utworzenie formy "podajnika" dla wektora slow
df = pd.DataFrame(dictionary, columns = ['words'])
df['counts'] = 0
dictionary_wide = pd.pivot(df, columns = 'words', values = 'counts').head(1).fillna(0)
dictionary_wide.shape


# wypelnienie "podajnika"
X = []
for i in range(len(Dataset)):
    
    # oczyszczenie tweeta
    tweet = tokenize(Dataset.text[i])
    tweet_sb = [snowball.stem(w) for w in tweet]
    tweet_sw = [w for w in tweet_sb if w not in stop_words]
    
    # forma
    document = copy.deepcopy(dictionary_wide)
    
    # zliczenie slow
    for _ in tweet_sw:
        document[_] += 1
    
    # dodanie do x-ow
    X.append(document.values[0])
    
X = np.array(X)
y = np.array(Dataset.target)

# Dataset.text[0]
# tst = pd.melt(document).sort_values('value', ascending = False)
X[0][np.newaxis, ...]
y[0][np.newaxis, ...]

X_tst = X[0:2][np.newaxis, ...]
y_tst = y[0:2][np.newaxis, ...]
np.unique(X_tst)
# %% uczenie =====================================================================================================
learned_params = learn_params(X, y)
e_step(X_tst, learned_params) # długo mieli
m_step(X_tst, learned_params)

def e_step(x, params):
    np.log([stats.multivariate_normal(params["mu_0"], params["sigma_0"]).pdf(x),
            stats.multivariate_normal(params["mu_1"], params["sigma_1"]).pdf(x)])
    log_p_y_x = np.log([1-params["phi"], params["phi"]])[np.newaxis, ...] + \
                np.log([stats.multivariate_normal(params["mu_0"], params["sigma_0"]).pdf(x),
            stats.multivariate_normal(params["mu_1"], params["sigma_1"]).pdf(x)]).T
    log_p_y_x_norm = special.logsumexp(log_p_y_x, axis=1)
    return log_p_y_x_norm, np.exp(log_p_y_x - log_p_y_x_norm[..., np.newaxis])


forecasts, posterior, loglikelihoods = run_EM(X_tst, learned_params, 000.1, 10)

# singular matrix error (macierz osobliwa - nieodwracalna) - po kwadransie ...
# https://stats.stackexchange.com/questions/219302/singularity-issues-in-gaussian-mixture-model

# TODO: Zoptymalizowac krok E oraz zmienic krok M na MAP
# 1. W kroku E liczymy rozklad prawdopodobienstwa korzystajac z funkcji gestosci.
# Potrzebujemy tego do policznia ze wzoru wartosci oczekiwanej danego punktu.
# Nastepenie wyliczamy wartosc oczekiwana dla kazdego punktu.
# (Skojarzyc sobie jak wyglada wzor na wartosc oczekiwana!)
# 2. W kroku M liczymy MAP

# PORZUCONE =============================================================================

# %% Brudnopis ===================================================================================================
import timeit
print("The time taken is ",timeit.timeit(stmt='a=10;b=10;sum=a+b'))

def e_step(x, params):
    p1 = stats.multivariate_normal(params["mu_0"], params["sigma_0"]).pdf(x)
    p2 = stats.multivariate_normal(params["mu_1"], params["sigma_1"]).pdf(x)

    # log_p_y_x = np.log([1-params["phi"], params["phi"]])[np.newaxis, ...] + \
    #             np.log([stats.multivariate_normal(params["mu_0"], params["sigma_0"]).pdf(x),
    #         stats.multivariate_normal(params["mu_1"], params["sigma_1"]).pdf(x)]).T
    # log_p_y_x_norm = special.logsumexp(log_p_y_x, axis=1)
    return p1,p2 #log_p_y_x_norm, np.exp(log_p_y_x - log_p_y_x_norm[..., np.newaxis])

X[0][np.newaxis, ...]
y[0][np.newaxis, ...]

stats.multivariate_normal(learned_params["mu_0"], learned_params["sigma_0"]).pdf(X[0][np.newaxis, ...]) # tu sobie ewidentnie nie radzi
stats.multivariate_normal.pdf(X[0][np.newaxis, ...], mean=learned_params["mu_0"], cov=learned_params["sigma_0"]) # tu tez

e_step(X, learned_params)

# MAP in python
# https://zhiyzuo.github.io/MLE-vs-MAP/#map
alpha = beta = 2
theta = 0.7
n = 50
X_arr = np.random.choice([0, 1], p=[1-theta, theta], size=n)
sum(X_arr) /  X_arr.size
X_arr.size
[alpha+sum(X_arr[:1+1]), beta+(i+1-sum(X_arr[:1+1]))]
np.asarray([[alpha+sum(X_arr[:i+1]), beta+(i+1-sum(X_arr[:i+1]))] for i in range(1)])
beta_arr = np.asarray([[alpha+sum(X_arr[:i+1]), beta+(i+1-sum(X_arr[:i+1]))] for i in range(X_arr.size)])
# Insert values along the given axis before the given indices.
np.insert(beta_arr, 0, [alpha, beta], 0)
beta_X = np.linspace(0, 1, 1000)
for iter_ in range(X_arr.size):
    a, b = beta_arr[iter_]
    beta_Y = stats.beta.pdf(x=beta_X, a=a, b=b)
plt.plot(beta_Y)



