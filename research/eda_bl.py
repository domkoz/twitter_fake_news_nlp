# -*- coding: utf-8 -*-
"""
Created on Fri Oct 14 20:31:30 2022

@author: Bartosz Lewandowski
"""
# %% libraries
import pandas as pd
import numpy as np
import re

from nltk.corpus import stopwords
from collections import Counter
# %% loading data
# Wszystko zgodne mimo problemów w samym pliku csv :)
Dataset = pd.read_csv("Dataset.csv", delimiter = ',')

# Removing '%20' from the middle and 's' from the end of keywords
# (removing 's' reduced set only by 7 keywords...)
Dataset.keyword = Dataset.keyword.str.replace(r'%20', ' ').str.rstrip('s')

# Check for any of characters not being alphanumeric
Dataset.assign(special = lambda x: x['keyword'].str.replace(' ', '').str.isalnum() == False)\
    .special.value_counts()  # no more nonalphanumeric values in keywords

Dataset
# =============================================================================
# id – identyfikator liczbowy danego twitta;
# text – treść twitta;
# location – lokalizacja, z której twitt został wysłany (puste w przypadku,
# gdy wysyłający nie miał udostępnionej lokalizacji w telefonie lub komputerze);
# keyword – wyróżnione słowa kluczowe (mogą być puste);
# target – w zbiorze treningowym, zmienna logiczna opisująca prawdziwość
# przekazywanej informacji (1), lub (0) dla fake newsów i informacji nieistotnych.
# =============================================================================

# %% exploration
len(Dataset[Dataset.target == 0]) # 4342 fake news
len(Dataset[Dataset.target == 1]) # 3271 real news

# number of localization in fake/real news
fake_loc = Dataset[Dataset.target == 0].location.value_counts()
len(Dataset[Dataset.target == 0]) - len(fake_loc) # 2200 NA's
# or
np.round(len(fake_loc)/len(Dataset[Dataset.target == 0])*100, 2) # 49.33%

real_loc = Dataset[Dataset.target == 1].location.value_counts()
len(Dataset[Dataset.target == 1]) - len(real_loc) # 1758 NA's
# or
np.round(len(real_loc)/len(Dataset[Dataset.target == 1])*100, 2) # 46.25%

# number and percentage of fake/real news per keywords
df1 = Dataset[['keyword','target']].groupby('keyword').count().reset_index()\
    .rename(columns={'target':'all'})
df2 = Dataset[['keyword','target']].groupby('keyword').sum().reset_index()\
    .rename(columns={'target':'real'})

df = pd.merge(df1,df2, on = 'keyword')\
    .assign(fake = lambda dataframe: dataframe['all'] - dataframe['real'])\
    .assign(fake_perc = lambda dataframe: np.round(
        (dataframe['fake']/dataframe['all'])*100,2))\
        .sort_values('fake_perc', ascending = False)
df

# percentage of fake/real news for missing keywords
df_na = Dataset[['keyword','target']][Dataset['keyword'].isna()] # 61
np.round(len(df_na[df_na.target == 0])/len(df_na)*100, 2) # 31.15% fake

# %% NLTK pre-processing

# tokenizing function with removing all-nonalphanumeric data
def tokenize(text):
    # set as low
    text = text.lower()
    # remove all non-alphanumeric characters
    text = re.sub(r'[^a-zA-Z 0-9]', ' ', str(text)) # przechodzac (od lewej) po ciagu znakow, te które nie zostały wskazane w 'pattern' zamienia na wskazana wartosc 'repl'
    # return iterable list of characters
    return text.split()

stop_words = set(stopwords.words('english'))

tweet = tokenize(Dataset.iloc[1,3])

filtered_tweet = [w for w in tweet if w not in stop_words]
filtered_tweet

def count(docs):
    
    word_counts = Counter()
    appears_in = Counter()
    total_docs = len(docs)
    
    # for every document, 
    for doc in docs:
        # Attention! By update(), Counter() class add together values, where dict.update() overwrite values.
        word_counts.update(doc)
        # set() - return only keys, where Counter() count them, ie. we count appearing of elements
        appears_in.update(set(doc))
    
    # create list of tuples: (word, count)
    temp = list(zip(word_counts.keys(), word_counts.values()))
    
    # word and count columns
    wc = pd.DataFrame(temp, columns = ['word', 'count'])
    
    # rank column (pd.df.rank() - Compute numerical data ranks (1 through n) along axis)
    wc['rank'] = wc['count'].rank(method = 'first', ascending = False)
    
    # percent total column
    total = wc['count'].sum()
    wc['pct_total'] = wc['count'].apply(lambda x: x / total)
    
    # cumulative percent total column (pd.df.cumsum() - Return cumulative sum over a DataFrame
    # or Series axis. Cumulative means [1, 2, 3, 4] -> [1, 1+2=3, 3+3=6, 6+4=10])
    wc = wc.sort_values(by = 'rank')
    wc['cul_pct_total'] = wc['pct_total'].cumsum()
    
    # appears in column
    t2 = list(zip(appears_in.keys(), appears_in.values()))
    ac = pd.DataFrame(t2, columns = ['word', 'appears_in'])
    wc = ac.merge(wc, on = 'word')
    
    # appears in percent column
    wc['appears_in_pct'] = wc['appears_in'].apply(lambda x: x / total_docs)
    
    return wc.sort_values(by='rank')

count([filtered_tweet])

documents = []
for tweet in Dataset.iloc[:,3]:
    temp = [w for w in tokenize(tweet) if w not in stop_words]
    documents.append(temp)
    
df_words = count(documents) # done for now

# najdłuższe słowo (przed filtracją)
longest = ''
for word in df_words.word:
    if len(word) > len(longest):
        longest = word
len(longest)
