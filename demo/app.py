import numpy as np
import pandas as pd

import pickle
import copy
import math

from nltk.stem import SnowballStemmer
from nltk.corpus import stopwords
from tokenize_custom import tokenize
from tabulate import tabulate

# load model
with open(r"data\Naive_Bayes_classifier.pkl", "rb") as fid:
    mNB = pickle.load(fid)

# load dictionary
dictionary = pd.read_parquet("data\dictionary.parquet", engine="pyarrow")
dictionary["counts"] = 0
dictionary_wide = (
    pd.pivot(dictionary, columns="words", values="counts").head(1).fillna(0)
)

# write tweet
tweet = input("Write tweet: ")
print("\n", tabulate([tweet]), "\n")

# clear the tweet
snowball = SnowballStemmer("english")
stop_words = set(stopwords.words("english"))

clean_tweet = tokenize(tweet)
clean_tweet_primal = [snowball.stem(w) for w in clean_tweet]
tweet_x = [w for w in clean_tweet_primal if w not in stop_words]

# dictionary of probabilities
doc_prob0 = copy.deepcopy(dictionary_wide)
doc_prob1 = copy.deepcopy(dictionary_wide)
for _ in tweet_x:
    doc_prob0[_] += mNB.feature_log_prob_[0][doc_prob0.columns.get_loc(_)]
    doc_prob1[_] += mNB.feature_log_prob_[1][doc_prob1.columns.get_loc(_)]

prob = pd.DataFrame(
    {
        "fake news": doc_prob0.iloc[:, np.where(doc_prob0 != 0)[1]].apply(math.exp),
        "real news": doc_prob1.iloc[:, np.where(doc_prob1 != 0)[1]].apply(math.exp),
    }
)
print(
    "Prawdopodobienstwo występownia poszczególnych słów w podziale na klasy:\n",
    tabulate(prob, headers="keys", tablefmt="psql", disable_numparse=True),
)

# calculating the probability by hand
def manual_fit(ind: list):

    real = mNB.classes_[0]
    real_news_prob = mNB.class_log_prior_[real]
    for _ in ind:
        real_news_prob += mNB.feature_log_prob_[real][_]

    fake = mNB.classes_[1]
    fake_news_prob = mNB.class_log_prior_[fake]
    for _ in ind:
        fake_news_prob += mNB.feature_log_prob_[fake][_]

    return math.exp(real_news_prob), math.exp(fake_news_prob)


doc = copy.deepcopy(dictionary_wide)
for _ in tweet_x:
    doc[_] += 1

print(
    "\nPr(real)=",
    f"{manual_fit(np.where(doc > 0)[1])[0]:.50f}",
    "\nPr(fake)=",
    f"{manual_fit(np.where(doc > 0)[1])[1]:.50f}",
)

if np.argmax(manual_fit(np.where(doc > 0)[1])) == 1:
    result = "FAKE NEWS"
else:
    result = "REAL NEWS"

print("\nOstatecznie -> ", result)
